import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyDrawPanel extends JPanel{
	
	public int xpos, ypos,	radius;
	public String text = "";

	public MyDrawPanel()
	{
		super();  //we always call the super constructor first
		 xpos = 100;
		 ypos=100;
		 radius=40;
		 setBackground(Color.white);
	}
	
	@Override
	public void paintComponent(Graphics g)
	//and we override paintComponent() whenever we want to do some drawing
	{
		super.paintComponent(g);  //don't forget to call the super method
		g.fillOval(xpos, ypos, radius, radius);  //draw the circle
		g.drawString(text, 30, 30); //draw the text
	}
}
