
public class Account {

	public final double INTEREST_RATE = 0.035;
	public int accountNum;
	protected double balance;

	
	public Account()
	{
		
	}
	
	public Account(double startingBalance, int accNum)
	{
	//	INTEREST_RATE = 0.035;
		accountNum = accNum;
		balance = startingBalance;
	}
	
	public double getBalance()
	{
		return balance;
	}
	
	

	public double credit(double money) throws IllegalArgumentException
	{
		if(money<0)
		{
			IllegalArgumentException e = new IllegalArgumentException("money must be non-negative");
			    throw e;
		}
		
		balance += money;
		return balance;
	}
	
	
	public boolean debit(double money)
	{
		if( balance >= money)
		{
			balance -= money;
			return true;
		}
		return false;
	}

}
