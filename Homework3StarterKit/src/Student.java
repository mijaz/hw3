import java.io.Serializable;

/**
 * The Student class i part of our Data Model in the MVC design pattern.
 * It includes all fields that constitute a single 'Student' as well as
 * restrictions on those fields.  Notice all fields are private, so user's only
 * choice is to use the setter methods, which lets us insure ssn and year are valid.
 * 
 * You should not need to alter this class.
 *
 */
public class Student implements Serializable{
	
	private static final long serialVersionUID = -2961068427440769216L;
	private String name;
	private double gpa;
	 private int year;
	 private int ssn; 
	private String major;
	
	public Student() 
	{
		
	}
	
	/**
	 * 
	 * @param name
	 * @param gpa
	 * @param year
	 * @param ssn
	 * @param major
	 */
	public Student(String name, double gpa, int year, int ssn, String major)
	{
		this.name = name;
		this.gpa = gpa;
		this.setYear(year);
		this.major = major;
		set_ssn(ssn);
	}
	
	public void setMajor(String major)
	{
		this.major = major;
	}
	
	public String getMajor()
	{
		return major;
	}
	
	
	
	public void set_ssn(int i)
	{
		if(i>0 && i < 1000000000)
			ssn = i;
		else
			throw new IllegalArgumentException("SSN value is not valid.");
	}

	public int get_ssn()
	{
		return ssn;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	
	public double getGPA() {
		return gpa;
	}

	
	public void setGPA(double gpa) {
		if(gpa<0 || gpa>4)
			throw new IllegalArgumentException("GPA must be between 0 and 4 inclusive");
		this.gpa = gpa;
	}
	
	public void setYear(int year)
	{
		if(year < 1)
			throw new IllegalArgumentException("Year must be greater than 0");
		this.year=year;
	}
	
	public int getYear()
	{
		return this.year;
	}
	
	@Override
	public String toString()
	{
		return name+" "+gpa+" "+this.year+" "+this.ssn+this.major;
	}

	
}