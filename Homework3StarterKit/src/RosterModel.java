import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/**
 * The RosterModel class defines part of our Model in the MVC
 * design pattern.  All interaction with our core data will happen through
 * this class.  If we ever needed to change the way data is handled, maybe by
 * storing in a local file or database, we would only need to alter this class.
 * Our controller and views wouldn't need to be altered at all.
 * 
 */
public class RosterModel {

	/**
	 * tmpRoster will hold batches of Student objects that we download from the server
	 */
	private List<Student> roster = new ArrayList<Student>();
	/**
	 * used to perform http requests with the web service
	 */
    DefaultHttpClient  httpclient = new DefaultHttpClient();
    
    /**
     * The IP address of the webservice
     */
    private static final String SERVERHOST = "134.74.112.14";
    /**
     * The path to the webservice script on the server
     */
    private static final String SERVERPATH = "/cs221/StudentServer.php";
    /**
     * The server ip address and path combined as a url address
     */
    private static final String SERVERURL = "http://134.74.112.14/cs221/StudentServer.php";
	 
	/**
	 * Search the list of local results for the student with ssn and return it.
	 * 
	 * @param ssn the ssn to find
	 * @return the corresponding student or null if not found locally
	 */
	public Student getStudent(int ssn)
	{
		for(Student s: roster)
		{
			if(s.get_ssn() == ssn)
				return s;
		}
		return null;
	}
	

	/**
	 * Makes an HTTP GET request to the StudentServer to return all students.  
	 * This method is fully functional, you do not need to change it, but can use it as
	 * an example for how to finish getStudents(StudentConstraint)
	 * 
	 * @return A list of all Student objects 
	 */
	public List<Student> getAllStudents()
	{
		roster = new ArrayList<Student>();
		
		   
		    URIBuilder uriHelper = new URIBuilder();
		    uriHelper.setScheme("http");
		    uriHelper.setHost(SERVERHOST);
		    uriHelper.setPath(SERVERPATH);
		    uriHelper.setParameter("getAll", "true");
		   
		    HttpGet getRequest;
			Scanner in=null;
			try {
				getRequest = new HttpGet(uriHelper.build().toString());
			
				HttpResponse resp;
		
				resp = httpclient.execute(getRequest);
				
				in = new Scanner(new  InputStreamReader(resp.getEntity().getContent()));
			 
				while(in.hasNextLine())
				{
					Student tmpStudent = new Student();
					tmpStudent.set_ssn(Integer.parseInt(in.nextLine()));

					tmpStudent.setYear(Integer.parseInt(in.nextLine()));
					tmpStudent.setGPA( Double.parseDouble(in.nextLine()));

					tmpStudent.setName( in.nextLine());
					tmpStudent.setMajor (in.nextLine());
					roster.add(tmpStudent);
				} 
			
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
			if(in!=null)
				in.close();
			
		return roster;
	}
	
	
	/**
	 * Makes an HTTP GET request to the StudentServer to return all students
	 * subject to the search constraints.
	 * 
	 * TODO complete this method by instantiating a new array list of students and making a GET request to the
	 * web service to fetch all students matching the constraints.  Create student objects to hold the records
	 * returned by the web service, add them to the arraylist, and return it.
	 * 
	 * @param c The search constraints
	 * @return A list of Student objects matching the search constraints
	 */
	public List<Student> getStudents(StudentConstraint c)
	{
		roster = new ArrayList<Student>();
		
		return roster;
	}
	
	/**
	 * Makes an HTTP POST request to alter the info for the Student s.
	 * This method is fully functional and doesn't need to be changed.
	 * But, you can use this as an example on how to make a POST request
	 * and finish the add(Student) method.
	 * 
	 * @param s The student's information
	 * @return true on success, false on failure
	 */
	public boolean update(Student s){

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("name", s.getName()));
		nameValuePairs.add(new BasicNameValuePair("major", s.getMajor()));
		nameValuePairs.add(new BasicNameValuePair("gpa", Double.toString(s.getGPA())));

		nameValuePairs.add(new BasicNameValuePair("ssn", Integer.toString(s.get_ssn())));
		nameValuePairs.add(new BasicNameValuePair("year", Integer.toString(s.getYear())));
		nameValuePairs.add(new BasicNameValuePair("edit", "true"));
	    
	    DefaultHttpClient  httpclient = new DefaultHttpClient();
	
	    HttpPost httppost = new HttpPost(SERVERURL);

	    String result = null;
	    
	    try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			HttpResponse resp =	httpclient.execute(httppost);
	    	Scanner in = new Scanner(resp.getEntity().getContent());
	    	
	    	if(in.hasNext())
	    		result= in.next();
			
			in.close();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    if(result!=null && result.contentEquals("true"))
		     return true;
	    
	    return false;
	}
	
	/**
	 * Makes an HTTP POST request to add a new student to the StudentServer.
	 * 
	 * TODO complete this method by making the appropriate POST request to the web service,
	 * checking the result, and returning true if it succeeded, false otherwise.
	 * 
	 * @param s The student's information
	 * @return true on success, false on failure
	 */
	public boolean add(Student s){

		return false; 
	}
	
}
