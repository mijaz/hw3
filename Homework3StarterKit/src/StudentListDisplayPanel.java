import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This Class is responsible for displaying a list of student names and ssns.
 * It also generates an "Edit" and a "View" button for each student.
 * 
 * Clicking either of these triggers an event listener, which simply passes the student's
 * ssn to the masterController's callback.  (And the controller takes care of things after that)
 *
 * This class is fully functional, you should not need to alter it.  Clicking the "View" button doesn't work
 * because you need to fix the setStudentView(int ssn) method in the controller.
 */
public class StudentListDisplayPanel extends JPanel{

	
	private static final long serialVersionUID = 5493383344375212140L;
	private Controller masterController;
	/**
	 * Create an ActionListener to add to "View" buttons.  Each button has
	 * the corresponding student's ssn stored in it's "ActionCommand" Field
	 * We can access this in the event handler, which lets us use one instance of the 
	 * event handler for ALL of the view buttons instead of creating a different instance 
	 * for every button in the list
	 */
	ActionListener viewHandler = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			//Get the ssn from the event's "Action Command" (which is really just a string)
			Integer ssn = Integer.parseInt(e.getActionCommand());
			//Tell the controller that the user wants to view the student with this ssn
			StudentListDisplayPanel.this.masterController.setStudentView(ssn);
		}
		
	};
	
	/**
	 * Create an ActionListener to add to "Edit" buttons.  Each button has
	 * the corresponding student's ssn stored in it's "ActionCommand" Field
	 * We can access this in the event handler, which lets us use one instance of the 
	 * event handler for ALL of the view buttons instead of creating a different instance 
	 * for every button in the list
	 */
	ActionListener editHandler = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			Integer ssn = Integer.parseInt(e.getActionCommand());
			StudentListDisplayPanel.this.masterController.setStudentEditView(ssn);	
		}
		
	};
	
	/**
	 * Initially this view just shows a label saying "No results yet"
	 * and stores a reference to the controller.  The controller can
	 * populate the list view by calling setResults(List<Student>)
	 * 
	 * @param masterController reference to the controller
	 */
	public StudentListDisplayPanel(Controller masterController)
	{
		super();
		setBackground(Color.white);
		this.masterController = masterController;
		JLabel textPane;
		textPane = new JLabel();
		textPane.setText("No Results Yet!");
		add(textPane);
	}
	
	/**
	 * Dynamically generates a JLabel and 2 JButtons for each student in results
	 * and adds them to a GridBagLayout.  It also adds the appropriate Action Listener
	 * to each button that notifies the controller when the user wants 
	 * to view or edit a student in the list.
	 * 
	 * @param results List of the students to display
	 */
	public void setResults(List<Student> results)
	{
		//First clear this panel of any components that are already added
		this.removeAll();
		
		//Set the layout manager
		this.setLayout(new GridBagLayout());

		//studentLabel is used to hold a reference to each JLabel as we loop through to create each one
		JLabel studentLabel; 
		
		//editButton holds a reference to each new edit Button we create in the loop 
		JButton editButton;
		
		//viewButton holds a reference to each new view Button we create in the loop 
		JButton viewButton;
		int i;
		
		//c is used to hold references to the constraint objects we create
		GridBagConstraints c  ;
		
		
		/*
		 * The chunk of code below sets up an empty panel that will grow to fill all
		 * "extra" space in the StudentListDisplayPanel.  This is one way to get the 
		 * layout manager to push all the labels and buttons to the left side of the panel
		 */
		c = new GridBagConstraints();
		c.gridx=4;
		c.gridy=0;
		c.weighty = 0.2;
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.EAST;
		JPanel fill = new JPanel();
		fill.setBackground(getBackground());
		add(fill, c);
		
		//Now we loop through the results and create the labels and buttons we need
		for(i=0;i<results.size();i++)
		{
			//First we get the student to display
			Student s = results.get(i);
			
			//We create a new constraint for this student's details
			c = new GridBagConstraints();
			c.gridx=0;
			c.gridy=i;
			c.weighty = 0.2;
			c.weightx = 0;
			c.anchor = GridBagConstraints.WEST;
			
			//We create the label, set the string to display, and make sure the background color matches the panel background
			studentLabel = new JLabel(Integer.toString(s.get_ssn()) + " " + s.getName());
			studentLabel.setBackground(getBackground());
			
			//We add the label to this panel, subject to the constraint c that positions it.
			this.add(studentLabel, c);
			
			//We create the button for editing this student
			editButton = new JButton("Edit");
			//We set the action command for this button to the student's ssn.
			//The "ActionCommand" is just a string that gets passed to 
			//our ActionListener as part of the event object.  This let's
			//the ActionListener pass the correct ssn to the controller
			editButton.setActionCommand(Integer.toString(s.get_ssn()));
			editButton.addActionListener(editHandler);
			
			//Now we set up the constraint object that will position this button
			c = new GridBagConstraints();
			c.gridx=1;
			c.gridy=i;
			c.anchor = GridBagConstraints.WEST;
			c.weightx = 0;
			c.weighty = 0.1;
			//and we add it
			this.add(editButton, c);
			
			//Now we do the same thing for the view button.
			viewButton = new JButton("View");
			viewButton.setActionCommand(Integer.toString(s.get_ssn()));
			viewButton.addActionListener(viewHandler);
			c = new GridBagConstraints();
			c.gridx=2;
			c.gridy=i;
			c.weightx = 0;
			c.anchor = GridBagConstraints.WEST;
			c.weighty = 0.1;
			this.add(viewButton, c);
		}
		
	}
	 
	
}
