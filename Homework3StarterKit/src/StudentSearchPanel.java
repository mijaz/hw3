import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This is a View in our MVC pattern, it's shows a form to let the
 * user enter search constraints and 2 buttons, 1 for clearing the form,
 * and 1 for submitting the form.  
 */
public class StudentSearchPanel extends JPanel{
	
	private static final long serialVersionUID = -1919259732216456062L;

	private Controller masterController;
	
	private JTextField txtMinid;
	private JTextField txtMaxid;
	private JTextField txtMingpa;
	private JTextField txtMaxgpa;
	private JTextField txtMinyear;
	private JTextField txtMaxyear;
	private JTextField txtName;
	private JTextField txtMajor;
	private JCheckBox chckbxMajornameexact;
	private JCheckBox chckbxNameexact;
	private JButton btnClear;

	/**
	 * Instantiates this component and all subcomponents and positions them.  Adds action listeners to the buttons.
	 * 
	 * @param controller reference to the master controller so we can access callback functions.
	 */
	public StudentSearchPanel(Controller controller)
	{
		/*
		 * TODO Currently all subcomponents are just added to this panel with no layout constraints. 
		 * Fix it so the form looks good, try to match the screen shot provided.
		 */
		super();
		this.masterController = controller;
		setBackground(Color.WHITE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		setLayout(gridBagLayout);
		JLabel j = new JLabel("Student Search Form");
		j.setFont(new Font("Tahoma", Font.PLAIN, 20));
		j.setBackground(Color.WHITE);
		GridBagConstraints gbc = new GridBagConstraints();
		
		add(j, gbc);
		
		JLabel lblMin = new JLabel("Min");
		
		add(lblMin, gbc);
		
		JLabel lblMax = new JLabel("Max");
		
		add(lblMax, gbc);
		
		JLabel lblId = new JLabel("ID:");
		
		add(lblId, gbc);
		
		txtMinid = new JTextField();
		
		add(txtMinid, gbc);
		txtMinid.setColumns(10);
		
		txtMaxid = new JTextField();
		
		add(txtMaxid, gbc);
		txtMaxid.setColumns(10);
		
		JLabel lblGpa = new JLabel("GPA:");
		
		add(lblGpa, gbc);
		
		txtMingpa = new JTextField();
	
		add(txtMingpa, gbc);
		txtMingpa.setColumns(10);  
		
		txtMaxgpa = new JTextField();
		
		add(txtMaxgpa, gbc);
		txtMaxgpa.setColumns(10);
		
		JLabel lblYear = new JLabel("Year:");
		
		add(lblYear, gbc);
		
		txtMinyear = new JTextField();
	
		add(txtMinyear, gbc);
		txtMinyear.setColumns(10);
		
		txtMaxyear = new JTextField();
		
		add(txtMaxyear, gbc);
		txtMaxyear.setColumns(10);
		
		JLabel lblStudentName = new JLabel("Student Name:");
		
		add(lblStudentName, gbc);
		
		txtName = new JTextField();
	
		add(txtName, gbc);
		txtName.setColumns(10);
		
		chckbxNameexact = new JCheckBox("Exact Match Only");
	
		chckbxNameexact.setBackground(Color.WHITE);
		
		add(chckbxNameexact, gbc);
		
		JLabel lblMajor = new JLabel("Major:");
		
		add(lblMajor, gbc);
		
		txtMajor = new JTextField();
		
		add(txtMajor, gbc);
		txtMajor.setColumns(10);
		
		chckbxMajornameexact = new JCheckBox("Exact Match Only");

		chckbxMajornameexact.setBackground(Color.WHITE);
		
		add(chckbxMajornameexact, gbc);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * This event handler detects the button click, and currently uses
				 * the controller callback method to show all students.
				 * 
				 *  TODO Fetch the values from the search fields and store
				 *  in this student constraint.  Then pass it to the right 
				 *  call back in the controller
				 */
				StudentConstraint sc = new StudentConstraint();
				
				StudentSearchPanel.this.masterController.displayAll();
				
			}
		});
		GridBagConstraints gbc_btnSearch = new GridBagConstraints();
		gbc_btnSearch.insets = new Insets(0, 0, 5, 0);
		gbc_btnSearch.gridx = 2;
		gbc_btnSearch.gridy = 8;
		add(btnSearch, gbc_btnSearch);
		
		/**
		 * TODO add an action listener to btnClear that wipes all the fields
		 * on the form.  Since we aren't interacting with any underlying data
		 * and don't need to load any other views, this listener doesn't need
		 * to call anything in the controller.
		 */
		btnClear = new JButton("Clear");
	
		add(btnClear, gbc);
	}
	
}
