import java.util.List;

import javax.swing.JOptionPane;

/**
 * This is the point of entry for our application.  It instanciates our views and provides callbacks
 * those views can use to handle actions, like adding a new student, editing, etc...
 * In those callbacks, our Controller code doesn't act on the data directly, but just calls
 * the appropriate methods in our RosterModel class, then loads the appropriate view.
 */
public class Controller {
	
	/**
	 * this serves as a reference to our top level view, the main frame that
	 * contains all sub panels.
	 */
	MainFrame mainFrame;
	
	/**
	 * studentConstraint serves to hold a reference to a StudentConstraint object
	 * corresponding to the user's current search.
	 */
	StudentConstraint studentConstraint;

	/**
	 * this serves as a reference to our dataModel.  All calls to interact with the data
	 * on the website will happen through this reference.
	 */
	protected RosterModel dataModel;
	
	

	/**
	 * This is the point of entry for our application.  It boot straps a new
	 * instance of Controller class.
	 * @param args unused, no command line args for this application
	 */
	public static void main(String... args)
	{
		Runnable r = new Runnable(){
			@Override
			public void run()
			{
				new Controller();
			}
		};
		javax.swing.SwingUtilities.invokeLater(r);
	}
	
	/**
	 * Creates Instances of The model and top-level view (MainFrame)
	 */
	public Controller()
	{
		dataModel = new RosterModel();
		mainFrame = new MainFrame(this);
	}
	
	/**
	 * This is a callback function that will be called by views to indicate
	 * the user changed tabs.  The controller checks to see if the selected
	 * panel is the StudentListDisplay.  If it is, we call on the data model
	 * to make sure we don't display stale results, and set those results in the view.
	 * 
	 * TODO If we have a StudentConstraint saved and the selected panel is the Student List Panel, then we should re-query the data model and set the results in the List View to make sure the user sees the freshest data.
	 */
	public void notifyTabChanged()
	{
		
	}
	
	/**
	 * Calls on the data model to get a List of Students who match the criteria in s.  
	 * Then we call on the StudentListDisplayPanel to set the view contents and the TabPane
	 * to display the StudentListDisplayPanel.
	 * 
	 * TODO complete this method by querying the data model to get the search results, then
	 * displaying the StudentListDisplayPanel with the results.
	 * 
	 * @param s A StudentConstraint object representing how the fields of the student data
	 * should be searched.
	 */
	public void searchAndDisplay(StudentConstraint s)
	{
		this.studentConstraint = s;
		 
	}
	
	/**
	 * Calls on the data model to get a List of all Students in the database.  
	 * Then we call on the StudentListDisplayPanel to set the view contents and the TabPane
	 * to display the StudentListDisplayPanel.
	 */
	public void displayAll()
	{
		List<Student> results = dataModel.getAllStudents();
		mainFrame.listP.setResults(results);
		mainFrame.tabPane.setSelectedComponent(mainFrame.scrollPane);
	}
	
	/**
	 * A callback to alert the controller that it should fetch the student
	 * with the given ssn from the data model, call to the StudentDisplayPanel to 
	 * set the details, and call to the tab pane to show the Student Display panel
	 * 
	 * TODO complete this method by querying the data model to get the student, then displaying the student in the StudentDisplayPanel.
	 * 
	 * @param ssn ssn of the student to display
	 */
	public void setStudentView(int ssn)
	{
		
	}
	
	/**
	 * A callback to alert the controller that it should fetch the student
	 * with the given ssn from the data model, call to the StudentEditPanel to 
	 * set the details, and call to the tab pane to show the Student Display panel
	 * 
	 * @param ssn ssn of the student to display
	 */
	public void setStudentEditView(int ssn)
	{
		mainFrame.editP.setStudent(dataModel.getStudent(ssn));
		mainFrame.tabPane.setSelectedComponent(mainFrame.editP);
	}
	
	/**
	 * A callback for editing a student.  We will call on the dataModel to update the student,
	 * and if successful, call the StudentDisplayPanel to set the student details and display it.
	 * If the update fails, we display an error message.
	 * @param s A Student object containing the altered student's details.
	 */
	public void editStudent(Student s)
	{
		if(dataModel.update(s))
		{
			mainFrame.dispP.setStudentDetails(s);
			mainFrame.tabPane.setSelectedComponent(mainFrame.dispP);
		}
		else
		{
			JOptionPane.showMessageDialog(mainFrame.dispP, "Update operation failed, sorry :(");
		}
	}
	
	/**
	 * A callback for creating a student.  We will call on the dataModel to add the student,
	 * and if successful, call the StudentDisplayPanel to set the student details and display it.
	 * If add operation fails, we display an error message.
	 * 
	 * TODO Call the data model to add this student to the web server.  On success, it should display the new student by loading the StudentDisplayPanel.  On failure, show an error message
	 * 
	 * @param s A Student object containing the new student's details.
	 */
	public void addStudent(Student s)
	{
		if(false)  //Call the data model and check result here.
		{

		}
		else
		{
			JOptionPane.showMessageDialog(mainFrame.dispP, "Uploading new student failed, sorry :(");
		}
	}
	
	
}
