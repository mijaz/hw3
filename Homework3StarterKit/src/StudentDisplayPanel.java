import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;

/**
 * This is a View class in the MVC pattern.  It is responsible for taking a
 * single student object and displaying the details nicely.
 * 
 * The layout is set up for you already.  The only thing you need to do
 * is complete the setStudentDetails(Student s) method below. 
 * 
 */
public class StudentDisplayPanel extends JPanel{


	/**
	 * Generated version id for serialization
	 */
	private static final long serialVersionUID = 4222060145653248410L;

	/**
	 * As of now, the StudentDisplayPanel doesn't give the user access to any actions,
	 * so we don't really need a reference to our master controller.  But, if we ever
	 * wanted to build in more features it might be useful.  So in keeping with our MVC pattern,
	 * ALL of our Views get a reference to the controller.
	 */
	@SuppressWarnings("unused")
	private Controller masterController;
	
	private JLabel header;
	private JLabel lblName;
	private JLabel lblSSN;
	private JLabel lblYear;
	private JLabel lblGPA;
	private JLabel lblMajor;
	
	public StudentDisplayPanel( Controller masterController){
		super();
		setBackground(Color.WHITE);
		
		this.masterController =  masterController;
		 GridBagLayout gridBagLayout = new GridBagLayout();
		 gridBagLayout.columnWidths = new int[]{187, 75, 0};
		 gridBagLayout.rowHeights = new int[]{14, 0, 0, 0, 0, 0, 0};
		 gridBagLayout.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		 gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		 setLayout(gridBagLayout);
		header = new JLabel("Student Display");
		header.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_header = new GridBagConstraints();
		gbc_header.insets = new Insets(0, 0, 5, 0);
		gbc_header.anchor = GridBagConstraints.NORTHWEST;
		gbc_header.gridx = 1;
		gbc_header.gridy = 0;
		add(header, gbc_header);
		
		JLabel lblNamePrompt = new JLabel("Name:");
		GridBagConstraints gbc_lblNamePrompt = new GridBagConstraints();
		gbc_lblNamePrompt.insets = new Insets(0, 0, 5, 5);
		gbc_lblNamePrompt.gridx = 0;
		gbc_lblNamePrompt.gridy = 1;
		add(lblNamePrompt, gbc_lblNamePrompt);
		
		lblName = new JLabel("");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 0);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 1;
		add(lblName, gbc_lblName);
		
		JLabel lblSsnPrompt = new JLabel("SSN:");
		GridBagConstraints gbc_lblSsnPrompt = new GridBagConstraints();
		gbc_lblSsnPrompt.insets = new Insets(0, 0, 5, 5);
		gbc_lblSsnPrompt.gridx = 0;
		gbc_lblSsnPrompt.gridy = 2;
		add(lblSsnPrompt, gbc_lblSsnPrompt);
		
		lblSSN = new JLabel("");
		GridBagConstraints gbc_lblSSN = new GridBagConstraints();
		gbc_lblSSN.insets = new Insets(0, 0, 5, 0);
		gbc_lblSSN.gridx = 1;
		gbc_lblSSN.gridy = 2;
		add(lblSSN, gbc_lblSSN);
		
		JLabel lblYearPrompt = new JLabel("Year:");
		GridBagConstraints gbc_lblYearPrompt = new GridBagConstraints();
		gbc_lblYearPrompt.insets = new Insets(0, 0, 5, 5);
		gbc_lblYearPrompt.gridx = 0;
		gbc_lblYearPrompt.gridy = 3;
		add(lblYearPrompt, gbc_lblYearPrompt);
		
		lblYear = new JLabel("");
		GridBagConstraints gbc_lblYear = new GridBagConstraints();
		gbc_lblYear.insets = new Insets(0, 0, 5, 0);
		gbc_lblYear.gridx = 1;
		gbc_lblYear.gridy = 3;
		add(lblYear, gbc_lblYear);
		
		JLabel lblGpaPrompt = new JLabel("GPA:");
		GridBagConstraints gbc_lblGpaPrompt = new GridBagConstraints();
		gbc_lblGpaPrompt.insets = new Insets(0, 0, 5, 5);
		gbc_lblGpaPrompt.gridx = 0;
		gbc_lblGpaPrompt.gridy = 4;
		add(lblGpaPrompt, gbc_lblGpaPrompt);
		
		lblGPA = new JLabel("");
		GridBagConstraints gbc_lblGPA = new GridBagConstraints();
		gbc_lblGPA.insets = new Insets(0, 0, 5, 0);
		gbc_lblGPA.gridx = 1;
		gbc_lblGPA.gridy = 4;
		add(lblGPA, gbc_lblGPA);
		
		JLabel lblMajorPrompt = new JLabel("Major:");
		GridBagConstraints gbc_lblMajorPrompt = new GridBagConstraints();
		gbc_lblMajorPrompt.insets = new Insets(0, 0, 0, 5);
		gbc_lblMajorPrompt.gridx = 0;
		gbc_lblMajorPrompt.gridy = 5;
		add(lblMajorPrompt, gbc_lblMajorPrompt);
		
		lblMajor = new JLabel("");
		GridBagConstraints gbc_lblMajor = new GridBagConstraints();
		gbc_lblMajor.gridx = 1;
		gbc_lblMajor.gridy = 5;
		add(lblMajor, gbc_lblMajor);
		
	}
	
	/**
	 * Method to be called by controller when the user wants to display
	 * a particular student.
	 * 
	 * TODO complete this method by setting the labels to display this students info
	 * 
	 * @param s The student to be displayed.
	 */
	public void setStudentDetails(Student s)
	{
		
	}
	
}
