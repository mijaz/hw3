/**
 * This very simple class just represents a set of constraints to be used
 * when searching for students.  So, it is a very small piece of our Data Model in the
 * MVC pattern.  The default values correspond to searching for all students.
 * 
 * You should not need to edit this class.
 */
public class StudentConstraint {
	/**
	 * The minimum SSN for students in this search
	 */
	public int minSSN = -1;
	/**
	 * The maximum SSN for students in this search.
	 * A value of -1 implies we don't want to have an upper limit on SSN
	 */
	public int maxSSN = -1;
	/**
	 * The name, or part of a name we are searching for.
	 */
	public String name = "";
	/**
	 * Set to true if the name string must be an exact match.
	 * If false, the web service returns students whose names include
	 * StudentConstraint.name as a substring
	 */
	public boolean nameExact = false;
	/**
	 * Minimum GPA for students included in our search results
	 */
	public double minGPA = 0;
	/**
	 * Maximum GPA for students included in our search results
	 */
	public double maxGPA = 4;
	/**
	 * Minimum Year for students included in our search results
	 */
	public int minYear = 1;
	/**
	 * Maximum Year for students included in our search results
	 */
	public int maxYear = -1;
	/**
	 * Major, or part of the major for students included in our search results
	 */
	public String majorName = "";
	/**
	 * Set to true if the major string must be an exact match.
	 * If false, the web service returns students whose majors include
	 * StudentConstraint.majorName as a substring
	 */
	public boolean majorNameExact = false;
}
