import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * MainFrame is the top level view in our application.  It creates all the subpanels
 * needed and adds them to itself, or a nested container.  You should not need to edit this file.
 * @author Andy
 *
 */
public class MainFrame extends JFrame { 
	
	/**
	 * Generated version id for serialization
	 */
	private static final long serialVersionUID = -4905184030623336230L;

	/**
	 * This holds a reference to the controller.
	 */
	Controller masterController;
	
	/**
	 * Reference used to access tab pane sub-view
	 */
	JTabbedPane tabPane;
	/**
	 * Reference used to access tab pane sub-view
	 */
	JScrollPane scrollPane;
	/**
	 * Reference used to access search panel sub-view
	 */
	StudentSearchPanel searchP;
	/**
	 * Reference used to access Student Display panel sub-view
	 */
	StudentDisplayPanel dispP;
	/**
	 * Reference used to access Student Edit panel sub-view
	 */
	StudentEditPanel editP;
	/**
	 * Reference used to access Add Student Panel sub-view
	 */
	StudentAddPanel addP;
	/**
	 * Reference used to access Student List Display panel sub-view
	 */
	StudentListDisplayPanel listP;

	/**
	 * Builds Main Application frame, instanciating and positioning all subcomponents.
	 * 
	 * @param controller Reference to the master controller so our views can access the callback
	 * functions in the controller.
	 */
	public MainFrame(Controller controller)
	{
		
		super("Student Management");
		this.masterController = controller;
		getContentPane().setBackground(Color.WHITE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);
		this.setBackground(Color.white);
		
		
		//Instanciate all child views
		tabPane = new JTabbedPane();
		tabPane.setBackground(Color.WHITE);
		searchP = new StudentSearchPanel(masterController);
		dispP = new StudentDisplayPanel(masterController);
		editP = new StudentEditPanel(masterController);
		addP = new StudentAddPanel(masterController);
		listP = new StudentListDisplayPanel(masterController);
		
		//Nest our list panel inside a scroll pane to get scroll bars
		scrollPane = new JScrollPane(listP,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		//add the child views to the tab pane
		tabPane.addTab("Search", searchP);
		tabPane.addTab("Display", dispP);
		tabPane.addTab("Edit", editP);
		tabPane.addTab("Add", addP);
		tabPane.addTab("List", scrollPane);
		
		//add a listener to the tab pane that notifies the controller....
		//the controller will query the data model for the latest information
		//to insure the data displayed in the newly selected view is fresh.
		tabPane.addChangeListener(new ChangeListener() {
	        public void stateChanged(ChangeEvent e) {
	         masterController.notifyTabChanged();
	        }
	      });
		
		//Now add the scroll pane to MainFrame
		this.getContentPane().add(tabPane);
		
		//And make it visible...
		this.setVisible(true);
	}
}
