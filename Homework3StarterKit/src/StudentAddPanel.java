import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 * This Class is responsible for displaying a form allowing details of a new student
 * to be input.  On form submission, this View just collects the details, puts them into 
 * a new Student object, and passes that to the Controller.
 * 
 */
public class StudentAddPanel extends JPanel{


	
/**
 * holds a reference to our main controller object
 */
	private Controller masterController;
	private JTextField nameField;
	private JTextField ssnField;
	private JTextField yearField;
	private JTextField gpaField;
	private JTextField majorField;
	private final JButton addButton = new JButton("Add");
	private static final long serialVersionUID = -3748152957839561001L;
	
	/**
	 * Instantiates and positions all components needed for this panel, adds an
	 * event listener to the "Add" button, and stores a reference to the master controller
	 * so we can access it's callback functions when the user clicks.
	 * 
	 * @param masterController Holds reference to master controller object
	 */
	public StudentAddPanel(Controller masterController)
	{
		/*
		 * TODO The first few components are added with GridBagLayoutConstraints.  The rest are just added,
		 * and the resulting display looks awful.  Add GridBagLayoutConstraints so the components are all
		 * positioned nicely.  Try to match the screen shot I provided.  You can do this by manually editing the code,
		 * or by using the WindowBuilder plug-in.
		 */
		super();
		setBackground(Color.WHITE);
		this.masterController =  masterController;
		GridBagLayout gridBagLayout = new GridBagLayout();
		
		setLayout(gridBagLayout);
		JLabel j = new JLabel("Student Edit Form");
		j.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_j = new GridBagConstraints();
		gbc_j.insets = new Insets(0, 0, 5, 0);
		gbc_j.anchor = GridBagConstraints.NORTH;
		gbc_j.gridx = 1;
		gbc_j.gridy = 0;
		add(j, gbc_j);
		
		JLabel lblStudentName = new JLabel("Student Name:");
		GridBagConstraints gbc_lblStudentName = new GridBagConstraints();
		gbc_lblStudentName.anchor = GridBagConstraints.EAST;
		gbc_lblStudentName.insets = new Insets(0, 0, 5, 5);
		gbc_lblStudentName.gridx = 0;
		gbc_lblStudentName.gridy = 1;
		add(lblStudentName, gbc_lblStudentName);
		
		nameField = new JTextField();
		GridBagConstraints gbc_nameField = new GridBagConstraints();
		gbc_nameField.insets = new Insets(0, 0, 5, 0);
		gbc_nameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_nameField.gridx = 1;
		gbc_nameField.gridy = 1;
		add(nameField, gbc_nameField);
		nameField.setColumns(10);
		
		JLabel lblSsn = new JLabel("SSN:");
	
		add(lblSsn);
		
		ssnField = new JTextField();
		ssnField.setColumns(10);
		
		JLabel lblYear = new JLabel("Year:");
		add(lblYear);
		
		yearField = new JTextField();
		
		add(yearField);
		yearField.setColumns(10);
		
		JLabel lblGpa = new JLabel("GPA:");
	
		add(lblGpa);
		
		gpaField = new JTextField();
		
		add(gpaField);
		gpaField.setColumns(10);
		
		JLabel lblMajor = new JLabel("Major:");
		
		add(lblMajor);
		
		majorField = new JTextField();
	
		add(majorField);
		majorField.setColumns(10);
		
		addButton.addActionListener(this.submitHandler);
		add(addButton);
	}
	
	
	/**
	 * An action listener whose actionPerformed method takes care of getting all the details
	 * about the new student from the form fields, puts them into a new student object, 
	 * and passes that student to the master controller's callback function for adding students.
	 * 
	 * Shows an error message if ssn, gpa, or year values are out of range or if name and/or major
	 * aren't specified.
	 */
	ActionListener submitHandler = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent event) {
			/** TODO Right now this event handler just shows a pointless message .
			 * You should remove the pointless message, then complete the
			 *  method so clicking the add button does something useful.
			 * You need to get the data from the form, put it into a new Student object,
			 * then pass the new student to the appropriate callback method in
			 * the controller
			 */
		     JOptionPane.showMessageDialog( masterController.mainFrame, "The action prformed method fired!");
		}
		
	};
	
}
