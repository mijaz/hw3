import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;


/**
 * This is a View class in the MVC pattern.  It is responsible for taking a
 * single student object and displaying the details in a form.  The user can
 * alter the details in any of the form fields except ssn which is disabled,
 * then submit the form to save changes.  The ActionListener for the submit button
 * just grabs the details from the form fields, puts them into a student object, and 
 * passes that to the master controller's editStudent callback.
 * 
 * This class is fully functional, you should not need to alter it.  On successful
 * submission, the controller loads the StudentDisplayPanel, which you need to fix so
 * it actually displays the student.  But nothing in this class needs to be changed.
 */
public class StudentEditPanel extends JPanel{


	private static final long serialVersionUID = -5901854105720824407L;
	private Controller masterController;
	private JTextField nameField;
	private JTextField ssnField;
	private JTextField yearField;
	private JTextField gpaField;
	private JTextField majorField;

	
	public StudentEditPanel(Controller masterController)
	{
		super();
		setBackground(Color.WHITE);
		this.masterController =  masterController;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{91, 47, 0};
		gridBagLayout.rowHeights = new int[]{14, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		JLabel j = new JLabel("Student Edit Form");
		j.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_j = new GridBagConstraints();
		gbc_j.insets = new Insets(0, 0, 5, 0);
		gbc_j.anchor = GridBagConstraints.NORTH;
		gbc_j.gridx = 1;
		gbc_j.gridy = 0;
		add(j, gbc_j);
		
		JLabel lblStudentName = new JLabel("Student Name:");
		GridBagConstraints gbc_lblStudentName = new GridBagConstraints();
		gbc_lblStudentName.anchor = GridBagConstraints.EAST;
		gbc_lblStudentName.insets = new Insets(0, 0, 5, 5);
		gbc_lblStudentName.gridx = 0;
		gbc_lblStudentName.gridy = 1;
		add(lblStudentName, gbc_lblStudentName);
		
		nameField = new JTextField();
		GridBagConstraints gbc_nameField = new GridBagConstraints();
		gbc_nameField.insets = new Insets(0, 0, 5, 0);
		gbc_nameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_nameField.gridx = 1;
		gbc_nameField.gridy = 1;
		add(nameField, gbc_nameField);
		nameField.setColumns(10);
		
		JLabel lblSsn = new JLabel("SSN:");
		GridBagConstraints gbc_lblSsn = new GridBagConstraints();
		gbc_lblSsn.anchor = GridBagConstraints.EAST;
		gbc_lblSsn.insets = new Insets(0, 0, 5, 5);
		gbc_lblSsn.gridx = 0;
		gbc_lblSsn.gridy = 2;
		add(lblSsn, gbc_lblSsn);
		
		ssnField = new JTextField();
		ssnField.setEnabled(false);
		ssnField.setEditable(false);
		GridBagConstraints gbc_ssnField = new GridBagConstraints();
		gbc_ssnField.insets = new Insets(0, 0, 5, 0);
		gbc_ssnField.fill = GridBagConstraints.HORIZONTAL;
		gbc_ssnField.gridx = 1;
		gbc_ssnField.gridy = 2;
		add(ssnField, gbc_ssnField);
		ssnField.setColumns(10);
		
		JLabel lblYear = new JLabel("Year:");
		GridBagConstraints gbc_lblYear = new GridBagConstraints();
		gbc_lblYear.anchor = GridBagConstraints.EAST;
		gbc_lblYear.insets = new Insets(0, 0, 5, 5);
		gbc_lblYear.gridx = 0;
		gbc_lblYear.gridy = 3;
		add(lblYear, gbc_lblYear);
		
		yearField = new JTextField();
		GridBagConstraints gbc_yearField = new GridBagConstraints();
		gbc_yearField.insets = new Insets(0, 0, 5, 0);
		gbc_yearField.fill = GridBagConstraints.HORIZONTAL;
		gbc_yearField.gridx = 1;
		gbc_yearField.gridy = 3;
		add(yearField, gbc_yearField);
		yearField.setColumns(10);
		
		JLabel lblGpa = new JLabel("GPA:");
		GridBagConstraints gbc_lblGpa = new GridBagConstraints();
		gbc_lblGpa.anchor = GridBagConstraints.EAST;
		gbc_lblGpa.insets = new Insets(0, 0, 5, 5);
		gbc_lblGpa.gridx = 0;
		gbc_lblGpa.gridy = 4;
		add(lblGpa, gbc_lblGpa);
		
		gpaField = new JTextField();
		GridBagConstraints gbc_gpaField = new GridBagConstraints();
		gbc_gpaField.insets = new Insets(0, 0, 5, 0);
		gbc_gpaField.fill = GridBagConstraints.HORIZONTAL;
		gbc_gpaField.gridx = 1;
		gbc_gpaField.gridy = 4;
		add(gpaField, gbc_gpaField);
		gpaField.setColumns(10);
		
		JLabel lblMajor = new JLabel("Major:");
		GridBagConstraints gbc_lblMajor = new GridBagConstraints();
		gbc_lblMajor.anchor = GridBagConstraints.EAST;
		gbc_lblMajor.insets = new Insets(0, 0, 5, 5);
		gbc_lblMajor.gridx = 0;
		gbc_lblMajor.gridy = 5;
		add(lblMajor, gbc_lblMajor);
		
		majorField = new JTextField();
		GridBagConstraints gbc_majorField = new GridBagConstraints();
		gbc_majorField.insets = new Insets(0, 0, 5, 0);
		gbc_majorField.fill = GridBagConstraints.HORIZONTAL;
		gbc_majorField.gridx = 1;
		gbc_majorField.gridy = 5;
		add(majorField, gbc_majorField);
		majorField.setColumns(10);
		GridBagConstraints gbc_editButton = new GridBagConstraints();
		gbc_editButton.insets = new Insets(0, 0, 5, 0);
		gbc_editButton.anchor = GridBagConstraints.EAST;
		gbc_editButton.gridx = 1;
		gbc_editButton.gridy = 6;
		editButton.addActionListener(this.submitHandler);
		editButton.setEnabled(false);
		add(editButton, gbc_editButton);
		
		
		myText = new JTextField();
		GridBagConstraints gbc_myText = new GridBagConstraints();
		gbc_myText.insets = new Insets(0, 0, 5, 0);
		gbc_myText.fill = GridBagConstraints.HORIZONTAL;
		gbc_myText.gridx = 1;
		gbc_myText.gridy = 7;
		add(myText, gbc_myText);
		myText.setColumns(10);
	}
	
	/**
	 * This method sets the student edit panels text fields to the values contained in Student
	 * @param s the student details to display
	 */
	public void setStudent(Student s)
	{
		this.ssnField.setText(Integer.toString(s.get_ssn()));
		this.nameField.setText(s.getName());
		this.yearField.setText(Integer.toString(s.getYear()));

		this.gpaField.setText(Double.toString(s.getGPA()));
		this.majorField.setText(s.getMajor());
		editButton.setEnabled(true);
	
	}
	
	ActionListener submitHandler = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent event) {
			String name;
			int ssn;
			double gpa;
			int year;
			String major;
			
			try{
				ssn = Integer.parseInt(ssnField.getText());
				gpa = Double.parseDouble(gpaField.getText());
				year = Integer.parseInt(yearField.getText());
				
				name = nameField.getText();
				major = majorField.getText();
				
				if(year < 1  || year > 4 || gpa < 0 || gpa > 4 || ssn>999999999 || ssn < 100000000
						|| name.contentEquals("") || major.contentEquals(""))
				{
					throw new NumberFormatException();
				}

				
				Student s = new Student(name,  gpa,year, ssn, major);
				masterController.editStudent(s);
			}
			catch(NumberFormatException e)
			{
				JOptionPane.showMessageDialog(StudentEditPanel.this.masterController.mainFrame, "Year must be an integer greater than 0,\nssn a 9 digit integer that does not start with 0,\nand gpa must be a number from 0.0 to 4.0\nStudent name and major are required.");
			}
		}
		
	};
	private final JButton editButton = new JButton("Edit");
	private JTextField myText;
	
}
