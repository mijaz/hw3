import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UnitTester {
	
	Transaction t;
	DataManager d;
	
 
	@Before
	public void setUp() throws ParseException
	{
		t = new Transaction(123456789, Transaction.sdf.parse("2/15/13"), 235.75);
		File f = new File("./transactionlog.xml");
		d = new DataManager(f);
		
	}
	
	@Test
	public void constuctorTest() throws ParseException
	{
		assertEquals(235.75, t.amount, 0.000001);
		assertFalse("Amount should not be 0 after construction", t.amount==0);
		assertEquals(123456789, t.account);
		assertFalse("Account should not be 0 after construction", t.account==0);
		
		Date testDate =  Transaction.sdf.parse("2/15/13");
		assertTrue("Date should be 2/15/13", testDate.compareTo(t.date)==0);
		testDate =  Transaction.sdf.parse("1/1/70");
		assertFalse("Date should not be 1/1/70", t.date.compareTo(testDate)==0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void constructorExceptionTest()
	{
		Transaction shouldFail = new Transaction(5, new Date(), 4.3);
	}
	
	@Test
	public void getSetAmountTest()
	{
		t.setAmount(3.50);
		assertEquals(3.5, t.getAmount(), 0.000001);
		assertFalse("Amount should not be 0 here", 0== t.getAmount());
	}
	
	@Test
	public void getSetDateTest() throws ParseException
	{
		Date testDate = Transaction.sdf.parse("1/1/70");
		t.setDate(testDate);
		assertTrue("Date should be 1/1/70", testDate.compareTo(t.getDate())==0);
		testDate = Transaction.sdf.parse("1/1/71");
		assertFalse("Date should be 1/1/70", testDate.compareTo(t.getDate())==0);
	}
	
	@Test
	public void getSetAccountTest()
	{
		t.setAccount(123456789);
		assertTrue("Account number should be 123456789 after setAccount() call", 123456789== t.getAccount());
		assertFalse("Account num should NOT be 0", 0==t.getAccount());
	}
	
	@Test
	public void toStringTest() throws ParseException
	{
		t = new Transaction(111111111, Transaction.sdf.parse("6/10/78"), 1000.58);
		
		String s = t.toString();
		System.out.println("String is "+s);
		assertTrue("Transaction.toString didn't return expected result",
				s.compareTo("111111111 06/10/78 1000.58")==0);
		
		assertFalse("Transaction.toString returned empty string",
				s.compareTo("")==0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void accountExceptionTest()
	{
		t.setAccount(0);
	}
	
	@Test
	public void dataManagerMeanTest()
	{
		assertEquals(31.826667, d.getMean(), 0.00001);
	}
	
	@Test
	public void dataManagerMinTest()
	{
		assertEquals(6.4, d.getMin(), 0.00001);
	}
	
	@Test
	public void dataManagerMaxTest()
	{
		assertEquals(54.54, d.getMax(), 0.00001);
	}
	@Test
	public void dataManagerStdevTest()
	{
		assertEquals(19.746502, d.getStdev(), 0.00001);
	}
	
	@Test
	public void dataManagerStartDateTest() throws ParseException
	{
		Date testDate = Transaction.sdf.parse("3/23/11");
		assertTrue("Min date should be 03/23/11", testDate.compareTo(d.getStartDate())==0);
		
		testDate = Transaction.sdf.parse("4/23/2012");
		assertFalse("Min date should NOT be 04/23/2012", testDate.compareTo(d.getStartDate())==0);
	}
	
	@Test
	public void dataManagerEndDateTest() throws ParseException
	{
		Date testDate = Transaction.sdf.parse("01/27/13");
		assertTrue("Max date should be 01/27/13", testDate.compareTo(d.getEndDate())==0);
		
		testDate = Transaction.sdf.parse("4/23/2012");
		assertFalse("Max date should NOT be 04/23/2012", testDate.compareTo(d.getEndDate())==0);
	}
	
}



