import static org.junit.Assert.*;

import java.io.File;
import java.text.ParseException;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;


public class JUnitBonus {

	Transaction t;
	DataManager d;
	
	@Before
	public void setUp() throws ParseException
	{
		File f = new File("./transactionlog2.xml");
		d = new DataManager(f);
		d.printList();
		
	}

	@Test
	public void dataManagerMeanTest()
	{
		assertEquals( 220.548, d.getMean(), 0.00001);
	}
	
	@Test
	public void dataManagerMinTest()
	{
		assertEquals(6.4, d.getMin(), 0.00001);
	}
	
	@Test
	public void dataManagerMaxTest()
	{
		assertEquals(1000.0, d.getMax(), 0.00001);
	}
	@Test
	public void dataManagerStdevTest()
	{
		assertEquals(390.057070, d.getStdev(), 0.00001);
	}
	
	@Test
	public void dataManagerStartDateTest() throws ParseException
	{
		Date testDate = Transaction.sdf.parse("3/23/11");
		assertTrue("Min date should be 03/23/11", testDate.compareTo(d.getStartDate())==0);
		
		testDate = Transaction.sdf.parse("4/23/2012");
		assertFalse("Min date should NOT be 04/23/2012", testDate.compareTo(d.getStartDate())==0);
	}
	
	@Test
	public void dataManagerEndDateTest() throws ParseException
	{
		Date testDate = Transaction.sdf.parse("01/27/13");
		assertTrue("Max date should be 01/27/13", testDate.compareTo(d.getEndDate())==0);
		
		testDate = Transaction.sdf.parse("4/23/2012");
		assertFalse("Max date should NOT be 04/23/2012", testDate.compareTo(d.getEndDate())==0);
	}

}
