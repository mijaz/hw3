import java.awt.Color;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.io.File;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;


public class DisplayPanel extends JPanel {


	JLabel headerField = new JLabel("Choose a file to generate report.");

	JLabel dateField = new JLabel("");
	JLabel meanField = new JLabel("");

	JLabel minField = new JLabel("");

	JLabel maxField = new JLabel("");

	JLabel stdDevField = new JLabel("");
	
	public DisplayPanel() {
	super();
		setBackground(Color.white);
	
	/*TODO:  Set a GridLayout fro this panel with 6 rows and 1 column.  We will use this to position
	 * 		the JLabels in the panel.
	 */
		
	add(headerField);
/*
 *  TODO: WE have only added one of the JLabels we created so far.  If we use the others, we must add them 
 *  to a parent container, which in this case would be this DisplayPanel object.
 */
	
	}
	
	
	/*setFields function should be called when we want to set all the display
	 * fields based on the data contained in DataManager.
	 * TODO: Get the data we need in d and set all JLabels with that data.
	 */
	public void setFields(DataManager d)
	{

	}
	
	/*
	 * setError displays an error message if no valid Transactions were found in the input file.
	 * We should set the error message in the header label.  The file name we need to display can
	 * be obtained from the File object passed in.
	 * TODO: make this method set the error message using the JLabels in this class.
	 */
	public void setError(File file)
	{

	}
	
	
	/*This function needs to clear any JLabels with text and set the headerField
	 * to the default message "Choose a file to generate report"
	 */
	public void clear()
	{

	}



}
